# Vuesday Tuesday Netlificando

> Generated with `vue create` then [pushed to Gitlab](https://docs.gitlab.com/ee/gitlab-basics/create-project.html) as follows mutandis mutanda &mdash;

```

git push --set-upstream https://gitlab.com/bretonio/nonexistent-project.git master
git remote add origin https://gitlab.com/bretonio/nonexistent-project.git

```

An exercise in deploying Vue projects for Netlify. A [Quasar plugin](https://quasar.dev/start/vue-cli-plugin) is used.

## Project Setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
